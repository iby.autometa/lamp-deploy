output "elb-name" {
  value       = aws_elb.lamp-elb.name
  description = "The ID of the runner security group"
}

output "elb-dns" {
  value = aws_elb.lamp-elb.dns_name
  description = "DNS name of ELB"
}

output "elb-zone-id" {
  value = aws_elb.lamp-elb.zone_id
  description = "ELB Zone ID"
}

