output "rds_name" {
    value = aws_db_instance.lamp-rds.name
}

output "rds_username" {
    value = aws_db_instance.lamp-rds.username
}

output "rds_password" {
    value = aws_db_instance.lamp-rds.password
}

output "rds_endpoint" {
    value = aws_db_instance.lamp-rds.endpoint
}
