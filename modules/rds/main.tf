resource "random_id" "id" {
  byte_length = 8
}

resource "aws_db_instance" "lamp-rds" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "foo"
  password             = var.rds_password
  parameter_group_name = "default.mysql5.7"
  final_snapshot_identifier = "wordpress-db-final-snapshot-${random_id.id.hex}"
}
