variable "vpc-id" {
  type = string
  description = "The ID of the VPC"
  default = "vpc-36c0f850"
}

variable "sg_name" {
  type = string
  description = "Name of the security group"
  default = "lamp-sg"
}
