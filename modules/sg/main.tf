resource "random_id" "id" {
  byte_length = 8
}

#Create security group for rancher nodes
resource "aws_security_group" "lamp-sg" {
  name        = "var.sg_name-${random_id.id.hex}"
  description = "Allow inbound traffic inside the rancher cluster"
  vpc_id      = var.vpc-id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
    Name = "lamp-nodes-sg"
  }
}

