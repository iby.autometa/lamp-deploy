#!/bin/bash
sudo apt update
yes | sudo apt install apache2 mysql-server mysql-client php7.2 php7.2-dev
sudo service apache2 start
wget https://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz
sudo cp -r wordpress/* /var/www/html
sudo apt install php7.2-mysql
sudo chown -R www-data:www-data /var/www/html
cd /var/www/html
sudo cp wp-config-sample.php wp-config.php
sudo sed -i 's/database_name_here/rds_name/g' wp-config.php
sudo sed -i 's/username_here/rds_username/g' wp-config.php
sudo sed -i 's/password_here/rds_password/g' wp-config.php
sudo sed -i 's/localhost/rds_endpoint/g' wp-config.php
sudo service apache2 restart
