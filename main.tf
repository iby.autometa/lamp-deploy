resource "random_id" "id" {
  byte_length = 8
}

#Import the security group module
module "lamp-sg" {
  source        = "./modules/sg/"
  sg_name       = "lamp-sg"
}

#Import the elb module
module "lamp-elb" {
  source        = "./modules/elb/"
  elb_name = "lamp-elb"
}

#Import the rds module
module "lamp-rds" {
  source        = "./modules/rds/"
  rds_password = var.rds_password
}

data "template_file" "init" {
  template = "${file("./scripts/user_data.tpl")}"
  vars = {
    rds_name = "${module.lamp-rds.rds_name}"
    rds_username = "${module.lamp-rds.rds_username}"
    rds_endpoint = "${module.lamp-rds.rds_endpoint}"
#    rds_password = "${module.lamp-rds.rds_password}"
  }
}

#Create launch configuration for autoscaling group
resource "aws_launch_configuration" "lamp-launch-config" {
  name_prefix   = "lamp-launch-config-${random_id.id.hex}"
  image_id      = var.ami_id
  instance_type = var.instance_type
  user_data = base64encode(file(var.user_data_file))
  key_name = var.key_name
  security_groups = [module.lamp-sg.sg-id]
}

#Create autoscaling group
resource "aws_autoscaling_group" "lamp-asg" {
  name                      = "lamp-asg-${random_id.id.hex}"
  max_size                  = 1
  min_size                  = 1
  health_check_grace_period = 100
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = aws_launch_configuration.lamp-launch-config.name
  vpc_zone_identifier       = [lookup(var.subnets, "subnet01"), lookup(var.subnets, "subnet02")]

  initial_lifecycle_hook {
    name                 = "lifecycle-continue"
    default_result       = "CONTINUE"
    heartbeat_timeout    = 30
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  }
  depends_on = [
    module.lamp-rds
  ]
}

#Attach autoscaling group to elb
resource "aws_autoscaling_attachment" "lamp-ha-elb-attach" {
  autoscaling_group_name = aws_autoscaling_group.lamp-asg.id
  elb = module.lamp-elb.elb-name
}

#Create A record in R53
resource "aws_route53_record" "www" {
  zone_id = "Z09880241LG1NIHVDY9W7"
  name    = "www.autometallc.com"
  type    = "A"
#  ttl     = "300"
  alias {
    name                   = module.lamp-elb.elb-dns
    zone_id                = module.lamp-elb.elb-zone-id
    evaluate_target_health = true
  }
}

#output "instance_private_ip_addresses" {
#  value = {
#    for instance in aws_autoscaling_group.lamp-ha:
#    instance.id => instance.ip_address
#  }
#}
